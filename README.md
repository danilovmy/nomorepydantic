**Serializer Battles**


* to test GET
 **RUN** python manage.py test djanti.tests.TestGetApiView
* to test GET and POST
 **RUN** python manage.py test djanti.tests.TestSerializers

* log_out = True  # get outpu in console
* objects_count = 3 # objects counter for serialize
* add_errors = True # att objects with serialize errors

in settings add something like:

SECRET_KEY = 'my-suuper-puuper-secret-key'
---

**Contact**

Maxim Danilov maxim@wpsoft.at

