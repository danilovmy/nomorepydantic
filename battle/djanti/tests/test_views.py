from django.test import Client, TestCase
from django.utils.crypto import get_random_string
from django.utils.lorem_ipsum import random
from django.utils.timezone import now


from rest_framework.test import APIRequestFactory
from rest_framework.utils.json import json

from djanti.models import Organization
from djanti.views import OrganisationApiView, OrganisationUpdateApiView
from djanti.views import PydantedOrganisationApiView, PydantedOrganisationUpdateApiView
from djanti.views import OrmApiView, OrmUpdateApiView
from djanti.views import OrganizationsList, OrganisationSerializer, OrmSerializer, DjangoModelSerializer



log_out = False
objects_count = 300
add_errors = False



# python manage.py test djanti.tests.TestSerializers --keepdb
class TestGetApiView(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.manager = Organization.objects
        if cls.manager.count() <= 1:
            for __ in range(objects_count):
                cls.manager.get_or_create(domain=get_random_string(25))
        return super(TestGetApiView, cls).setUpClass()

    def test_restget(self):
        start_time = now()
        request = APIRequestFactory().get('/djanti/rest/?format=json', content_type='application/json')
        response = OrganisationApiView.as_view()(request)
        response.render()
        end_time = now() - start_time
        content = f'{response.content}'
        if not log_out:
            content = ''
        print('rest get ', self.manager.count(), 'by', end_time, content)

    def test_pydaget(self):
        start_time = now()
        request = APIRequestFactory().get('/djanti/pydantic/?format=json', content_type='application/json')
        response = PydantedOrganisationApiView.as_view()(request)
        response.render()
        end_time = now() - start_time
        content = f'{response.content}'
        if not log_out:
            content = ''
        print('Pydanted get ', self.manager.count(), 'by', end_time, content)

    def test_ormget(self):
        start_time = now()
        request = APIRequestFactory().get('/djanti/orm/?format=json', content_type='application/json')
        response = OrmApiView.as_view()(request)
        response.render()
        end_time = now() - start_time
        content = f'{response.content}'
        if not log_out:
            content = ''
        print('ORM get ', self.manager.count(), 'by', end_time, content)


# python manage.py test djanti.tests.TestSerializers --keepdb
class TestSerializers(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.manager = Organization.objects
        if cls.manager.count() <= 1:
            for __ in range(objects_count):
                __, __ = cls.manager.get_or_create(domain=get_random_string(25))
            if add_errors:
                cls.manager.filter(pk=cls.manager.last().pk).update(domain='')
        return super(TestSerializers, cls).setUpClass()

    def test_restpost(self):
        start_time = now()
        request = APIRequestFactory().get('/djanti/rest/?format=json', content_type='application/json')
        request.session = {}
        response = OrganisationApiView.as_view()(request)
        response.render()
        end_time = now() - start_time
        content = response.content
        if not log_out:
            content = ''
        print('RestGet', self.manager.count(), 'by', end_time, content)

        content = response.content
        start_time = now()
        serializer = OrganisationSerializer(data=json.loads(content), many=True)
        data = serializer.validated_data if serializer.is_valid() else serializer.errors
        data = '{}'.format(data)
        end_time = now() - start_time
        if not log_out:
            data = content = ''
        print('RestPost', self.manager.count(), 'by', end_time, content, data)

    def test_pydapost(self):
        start_time = now()
        request = APIRequestFactory().get('/djanti/pydantic/?format=json', content_type='application/json')
        response = PydantedOrganisationApiView.as_view()(request)
        response.render()
        end_time = now() - start_time
        content = f'{response.content}'
        if not log_out:
            content = ''
        print('PydaGet ', self.manager.count(), 'by', end_time, content)

        content = response.content
        start_time = now()
        try:
            data = OrganizationsList.parse_raw(content).dict()['__root__']
        except Exception as e:
            data = e.errors()
        data = '{}'.format(data)
        end_time = now() - start_time

        if not log_out:
            data = content = ''
        print('PydaPost', self.manager.count(), 'by', end_time, content, data)

    def test_ormpost(self):
        start_time = now()
        request = APIRequestFactory().get('/djanti/orm/?format=json', content_type='application/json')
        response = OrmApiView.as_view()(request)
        response.render()
        end_time = now() - start_time
        content = f'{response.content}'
        if not log_out:
            content = ''
        print('OrmGet', self.manager.count(), 'by', end_time, content)

        content = response.content
        start_time = now()
        serializer = OrmSerializer(data=json.loads(content), many=True)
        data = serializer.validated_data if serializer.is_valid() else serializer.errors
        data = '{}'.format(data)
        end_time = now() - start_time
        if not log_out:
            data = content = ''
        print('ORMPost', self.manager.count(), 'by', end_time, content, data)

    def test_model(self):
        start_time = now()
        request = APIRequestFactory().get('/djanti/orm/?format=json', content_type='application/json')
        response = OrmApiView.as_view()(request)
        response.render()
        end_time = now() - start_time
        content = f'{response.content}'
        if not log_out:
            content = ''
        print('ModelGet', self.manager.count(), 'by', end_time, content)

        content = response.content
        start_time = now()
        serializer = DjangoModelSerializer(data=json.loads(content), many=True)
        data = serializer.validated_data if serializer.is_valid() else serializer.errors
        data = '{}'.format(data)
        end_time = now() - start_time
        if not log_out:
            data = content = ''
        print('ModelPost', self.manager.count(), 'by', end_time, content, data)
