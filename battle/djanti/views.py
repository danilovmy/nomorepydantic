from djantic.main import ModelSchema, BaseModel, List
from pydantic.fields import Field

from pydantic.error_wrappers import ValidationError as Pyderror

from django.core.exceptions import ValidationError

from rest_framework.serializers import ModelSerializer, Serializer, ReturnList, IntegerField, CharField
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.parsers import JSONParser, FormParser, MultiPartParser
from django.utils.translation import ugettext_lazy as _

from django import forms
from .models import Organization

from itertools import chain


# ________ REST-API SOLUTION ___________________


class OrganisationSerializer(ModelSerializer):

    class Meta:
        model = Organization
        fields = ['id', 'domain']
        extra_kwargs = {'id': {'read_only': False}, 'domain': {'validators': []}}


class ApiMixIn:
    serializer_class = OrganisationSerializer
    permission_classes = (AllowAny,)
    queryset = Organization.objects.all()


class OrganisationApiView(ApiMixIn, ListAPIView):
    http_method_names = ['get']  # , 'post', 'put', 'patch', 'delete', 'head', 'options', 'trace'
    filter_backends = []

    def get_queryset(self):
        return super(OrganisationApiView, self).get_queryset().only('id', 'domain')


class OrganisationUpdateApiView(ApiMixIn, ListAPIView):
    http_method_names = ['get', 'post']  # , 'post', 'put', 'patch', 'delete', 'head', 'options', 'trace'
    filter_backends = []

    def post(self, *args, **kwargs):
        serializer = self.get_serializer(None, data=self.request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data)

# ________ PYDANTIC SOLUTION ___________________


# class OrganizationSchema(ModelSchema):

#     class Config:
#         model = Organization
#         include = ['id', 'domain']


class OrganizationSchema(BaseModel):
    id: int
    domain: str

    class Config:
        min_anystr_length = 1
        max_anystr_length = 25


class OrganizationsList(BaseModel):
    __root__: List[OrganizationSchema]

    class Config:
        validate_all = False


class PydantedOrganisationSerializers:

    def __init__(self, queryset=None, **kwargs):
        super(PydantedOrganisationSerializers, self).__init__()
        vars(self).update(queryset=queryset, kwargs=kwargs)

    @property
    def validated_data(self):
        pydanted = OrganizationSchema
        try:
            data = OrganizationsList(__root__=(pydanted(**kwargs) for kwargs in self.queryset.all().values('id', 'domain')))
        except Exception as error:
            data = error
        return data.json()

    data = validated_data

    def is_valid():
        return
        #return f"[{','.join((pydanted(**kwargs).json() for kwargs in self.queryset.all().values('id', 'domain')))}"


class MyJSONParser(JSONParser):

    def parse(self, stream, *args, **kwargs):
        """Convert the incoming bytestream as JSON and returns the resulting data."""
        return stream.body


class MyJSONRenderer(JSONRenderer):

    def render(self, data, *args, **kwargs):
        if not data or isinstance(data, ReturnList):
            return super(MyJSONRenderer, self).render(data, *args, **kwargs)
        # We always fully escape \u2028 and \u2029 to ensure we output JSON
        return data.replace('\u2028', '\\u2028').replace('\u2029', '\\u2029').encode()


class PydantedOrganisationApiView(OrganisationApiView):
    renderer_classes = [MyJSONRenderer, BrowsableAPIRenderer]
    serializer_class = PydantedOrganisationSerializers
    filter_backends = []


class PydantedOrganisationUpdateApiView(OrganisationUpdateApiView):
    renderer_classes = [MyJSONRenderer, BrowsableAPIRenderer]
    parser_classes = [MyJSONParser, FormParser, MultiPartParser]
    http_method_names = ['get', 'post']  # , 'post', 'put', 'patch', 'delete', 'head', 'options', 'trace'
    filter_backends = []

    def post(self, *args, **kwargs):
        try:
            data = OrganizationSchema.parse_raw(self.request.data)
        except Pyderror as error:
            data = error
        return Response(data.json())

# ________ DJANGO ORM ___________________


class OrmOrganisationSerializers(PydantedOrganisationSerializers):

    @property
    def data(self):
        return self.queryset.values('id', 'domain')


class OrmApiView(OrganisationApiView):
    filter_backends = []
    serializer_class = OrmOrganisationSerializers


class MyModelForm(forms.ModelForm):

    class Meta:
        fields = ['id', 'domain']
        model = Organization

    id = forms.IntegerField(label=_('auto key'), min_value=0, required=True)
    domain = forms.CharField(label=_('Domain'), max_length=25, required=True)

    def validate_unique(self):
        return



class OrmSerializer(object):

    def __init__(self, *args, **kwargs):
        self._data = kwargs.get('data') or {}

    def is_valid(self, *args, **kwargs):
        forms = (MyModelForm(data) for data in self._data)
        self.data = self.validated_data = [form.cleaned_data if form.is_valid() else form.errors for form in forms]
        return True


class OrmUpdateApiView(OrganisationUpdateApiView):
    serializer = OrmSerializer
    filter_backends = []


class DjangoModelSerializer(object):
    exclude = set(field.name for field in Organization._meta.fields if field.name not in ('id', 'domain'))

    def __init__(self, *args, **kwargs):
        self._data = kwargs.get('data') or {}

    def is_valid(self, *args, **kwargs):

        def data_yielder(full_data):
            for _data in full_data:
                try:
                    Organization(**_data).full_clean(exclude=self.exclude, validate_unique=False)
                except ValidationError as errors:
                    _data = errors
                yield _data

        self.data = self.validated_data = [data for data in data_yielder(self._data)]
        return True


class ModelUpdateApiView(OrmUpdateApiView):
    serializer = DjangoModelSerializer
    filter_backends = []
