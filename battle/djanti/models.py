from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from django.db.models import QuerySet, Q


class DomainValidator(RegexValidator):
    regex = r'^[a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9]$'
    message = _('Enter a valid value. Example: {value}')

    def __call__(self, value):
        try:
            super(DomainValidator, self).__call__(value)
        except ValidationError:
            raise ValidationError(self.message.format(value=slugify(value).replace('_', '-')), code=self.code, params={'value': value})


class OrganizationQuerySet(QuerySet):

    def get_by_domain(self, domain):
        for organization in self.filter(Q(domain=domain) if domain else Q(pk__isnull=True)):
            return organization


class Organization(models.Model):
    class Meta:
        verbose_name = _('Organization')
        verbose_name_plural = _('Organizations')
        ordering = ('id',)

    domain = models.CharField(_('Domain'), max_length=25, unique=True, validators=(DomainValidator(),))
    active = models.BooleanField(verbose_name=_('Active'), default=False)

    objects = OrganizationQuerySet.as_manager()

    def __str__(self):
        return self.domain or ''
