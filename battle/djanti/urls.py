from django.urls import path
from .views import  OrganisationApiView, OrganisationUpdateApiView
from .views import PydantedOrganisationApiView, PydantedOrganisationUpdateApiView
from .views import OrmApiView, OrmUpdateApiView
from .views import ModelUpdateApiView


urlpatterns = [
    path('rest/', OrganisationApiView.as_view(), name='drf'),
    path('pydantic/', PydantedOrganisationApiView.as_view(), name='pydantic'),
    path('orm/', OrmApiView.as_view(), name='orm'),
    path('model/', OrmApiView.as_view(), name='orm'),
    path('updrest/', OrganisationUpdateApiView.as_view(), name='upddrf'),
    path('updpydantic/', PydantedOrganisationUpdateApiView.as_view(), name='updpydantic'),
    path('updorm/', OrmUpdateApiView.as_view(), name='updorm'),
    path('updmodel/', ModelUpdateApiView.as_view(), name='updmodel'),
]
